import {Switch, Route} from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import News from "./containers/News/News";
import NewPost from "./containers/NewPost/NewPost";
import Post from "./containers/Post/Post";

const App = () => (
    <>
      <CssBaseline />
      <header>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={News} />
            <Route path="/news/post" component={NewPost}/>
              <Route path="/news/:id" component={Post}/>
          </Switch>
        </Container>
      </main>
    </>
);

export default App;