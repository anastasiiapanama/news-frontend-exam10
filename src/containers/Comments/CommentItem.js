import React from 'react';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import {makeStyles} from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: '8px',
        marginBottom: '15px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '120px',
        height: '120px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '8px',
        fontSize: '13px',
        padding: 0
    }
}));

const CommentItem = ({author, comment, deleteLink}) => {
    const classes = useStyles();

    if(!author) {
        author = 'Anonymous';
    };

    return (
        <Card className={classes.root}>
            <div className={classes.contentBlock}>
                <CardContent className={classes.content}>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        <strong>{author}</strong>
                    </Typography>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        <p>{comment}</p>
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton onClick={deleteLink}>
                        <DeleteOutlineIcon/>
                    </IconButton>
                </CardActions>
            </div>
        </Card>
    );
};

export default CommentItem;