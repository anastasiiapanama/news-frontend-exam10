import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {deleteComment, fetchComments} from "../../store/action";

import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import CommentItem from "./CommentItem";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Comments = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const comments = useSelector(state => state.comments);
    const loading = useSelector(state => state.newsLoading);

    useEffect(() => {
        dispatch(fetchComments());
    }, [dispatch]);

    const deleteCommentHandler = (e, id) => {
        e.preventDefault();

        dispatch(deleteComment(id));
    };

    return (
        <Grid item>
            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : comments.map(news => (
                <CommentItem
                    key={news.id}
                    id={news.id}
                    author={news.author}
                    comment={news.comment}
                    deleteLink={e => deleteCommentHandler(e, news.id)}
                />
            ))}
        </Grid>
    );
};

export default Comments;