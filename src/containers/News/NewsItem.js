import React from 'react';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {CardMedia} from "@material-ui/core";

import imageNotAvailable from '../../assets/images/imageNotAvailable.png';
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const NewsItem = ({title, date, image, id, deleteLink}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardContent>
                    <p style={{marginLeft: '10px'}}>
                        Date: {date}
                    </p>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/news/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                    <IconButton onClick={deleteLink}>
                        <DeleteOutlineIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default NewsItem;