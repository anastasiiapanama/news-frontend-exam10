import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

import {deleteNews, fetchNews} from "../../store/action";

import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import NewsItem from "./NewsItem";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const News = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const news = useSelector(state => state.news);
    const loading = useSelector(state => state.newsLoading);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);

    const deleteNewsHandler = (e, id) => {
        e.preventDefault();

        dispatch(deleteNews(id));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">News</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/news/post">Add new post</Button>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : news.map(news => (
                    <NewsItem
                        key={news.id}
                        id={news.id}
                        title={news.title}
                        date={news.date}
                        image={news.image}
                        deleteLink={e => deleteNewsHandler(e, news.id)}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default News;