import React from 'react';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
// import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const PostInfo = ({title, date, description}) => {
    const classes = useStyles();

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardContent>
                    <p style={{marginLeft: '10px'}}>
                        Date: {date}
                    </p>
                    <p style={{marginLeft: '10px'}}>
                        Description: {description}
                    </p>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default PostInfo;