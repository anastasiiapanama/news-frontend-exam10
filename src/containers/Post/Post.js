import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";

import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import {createComment, fetchOnePost} from "../../store/action";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Comments from "../Comments/Comments";
import CommentForm from "../../components/CommentForm/CommentForm";

const useStyles = makeStyles({
    card: {
        height: '100%',
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});

const Post = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const onePost = useSelector(state => state.onePost);
    const loading = useSelector(state => state.newsLoading);

    const [oneItem, setOneItem] = useState({});

    useEffect(() => {
        dispatch(fetchOnePost(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneItem(onePost);
    }, [onePost]);

    const onCommentFormSubmit = async commentData => {
        await  dispatch(createComment(commentData));
    };

    return (
        <Grid item container spacing={1}>
            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) :
                <Grid item xs={12} sm={12} md={6} lg={4}>
                    <Card className={classes.card}>
                        <CardHeader
                            style={{textAlign: "center"}}
                            title={oneItem.title}
                        />
                        <CardContent>
                            <p style={{marginLeft: '10px'}}>
                               <strong>Date:</strong> {oneItem.date}
                            </p>
                            <p style={{marginLeft: '10px'}}>
                                <strong>Description:</strong> {oneItem.description}
                            </p>
                            <p style={{marginLeft: '10px'}}>
                                <strong>Comments:</strong>
                            </p>
                            <Comments/>
                            <p style={{marginLeft: '10px'}}>
                                <strong>Add comment:</strong>
                            </p>
                            <CommentForm
                                onSubmit={onCommentFormSubmit}
                            />
                        </CardContent>
                    </Card>
                </Grid>
            }
        </Grid>
    )
};

export default Post;