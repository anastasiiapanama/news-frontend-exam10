import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const CommentForm = ({onSubmit}) => {

    const [state, setState] = useState({
        author: '',
        comment: ''
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    return (
        <form onSubmit={submitFormHandler}>
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id="author"
                        label="Author"
                        name="author"
                        value={state.author}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        multiline
                        rows={3}
                        variant="outlined"
                        id="comment"
                        label="Comment"
                        name="comment"
                        value={state.comment}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">
                        Add
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default CommentForm;