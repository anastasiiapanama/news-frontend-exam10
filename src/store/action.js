import axiosApi from "../axios-api";
import {NotificationManager} from "react-notifications";

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';

export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});

export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, post});

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});

export const fetchNews = () => {
    return async dispatch => {
        try {
            dispatch(fetchNewsRequest());

            const response = await axiosApi.get('/news');

            dispatch(fetchNewsSuccess(response.data));
        } catch (e) {
            dispatch(fetchNewsFailure());
            NotificationManager.error('Could not fetch news');
        }
    };
};

export const deleteNews = id => {
    return async dispatch => {
        try {
            await axiosApi.delete( '/news/' + id + ".json");

            dispatch(fetchNews());
        } catch (e) {
            console.log(e);
        }
    };
};

export const createNews = newsData => {
    return async dispatch => {
        await axiosApi.post('/news', newsData);
        dispatch(createNewsSuccess());
        dispatch(fetchNews());
    };
};

export const fetchOnePost = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/news/' + id + '.json');

            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchComments = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/comments');

            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            NotificationManager.error('Could not fetch news');
        }
    };
};

export const deleteComment = id => {
    return async dispatch => {
        try {
            await axiosApi.delete( '/comments/' + id + ".json");

            dispatch(fetchComments());
        } catch (e) {
            console.log(e);
        }
    };
};

export const createComment = commentData => {
    return async dispatch => {
        await axiosApi.post('/comments', commentData);
        dispatch(createCommentSuccess());
        dispatch(fetchComments());
    };
};