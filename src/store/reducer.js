import {
    FETCH_COMMENTS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    FETCH_POST_SUCCESS
} from "./action";

const initialState = {
    news: [],
    onePost: {},
    comments: [],
    newsLoading: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, newsLoading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, newsLoading: false, news: action.news};
        case FETCH_NEWS_FAILURE:
            return {...state, newsLoading: false};
        case FETCH_POST_SUCCESS:
            return {...state, onePost: action.post};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, newsLoading: false, comments: action.comments};
        default:
            return state;
    }
};

export default reducer;